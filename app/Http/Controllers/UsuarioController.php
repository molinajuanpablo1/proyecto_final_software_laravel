<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use PHPUnit\Runner\Exception;
use App\UsuarioModel;
use Illuminate\Http\Request;
use App\Http\BL\UsuarioBL;
use Validator;
//use Tymon\JWTAuth\Contracts\Validator;


class UsuarioController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/usuarios",
     *     tags={"Usuarios"},
     *     summary="Api para el despliegue de todos los usuarios",
     *     @OA\Response(
     *         response=200,
     *         description="Listado de los usuarios exitosa"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ohhh NO!! Ha ocurrido un error en los Usuarios."
     *     )
     * )
     */
    public function list(Request $request)
    {
        $UsuarioBL = new UsuarioBL();
        $Response = null;
        if (isset($request->nombre)) {
            $Response = $UsuarioBL->pedirUsuario($request, 'buscar');
        } else {
            $Response = $UsuarioBL->pedirUsuario($request, 'obtener');
        }
        return $Response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Post(
     * 		path="/api/usuarios/create",
     *      summary="Crear",
     * 		tags={"Usuarios"},
     * 		operationId="CreacionUsuario",
     * 		summary="Api documentada para la creacion de un usuario nuevo",
     *      @OA\Parameter(
     *           name="ci",
     *           in="path",
     *           required=true,
     *            @OA\Schema(
     *              type="string",
     *              format="int32"
     *            ),
     *            style="form"
     *          ),
     *      @OA\Response(
     *              response=200,
     *              description="Creacion del usuario exitosa"
     *        ),
     *       @OA\Response(
     *            response="default",
     *            description="Ohhh NO!! Error al crear la cuenta"
     * 	    	),
     * 	)
     *
     */
    public function create(Request $request)
    {
        $rules = [
            'ci'=>'required',
            'password'=>'required',
            'nickName'=>'bail|required|max:50',
            'carrera'=>'bail|required|max:30',
            'apellido_p'=>'bail|required|max:50',
            'apellido_m'=>'bail|required|max:50',
            'email'=>'bail|required|max:200',
            'nombre'=>'bail|required|max:50',
            'rol'=>'bail|required|max:45',
        ];
        $msg = [
            'ci.required' => 'El CI de usuario es requerido',
            'password.required' => 'El password del usuario es requerido',
            'nickName.required' => 'Campo requerido',
            'nickName.max' => 'Contenido de nickname no debe tener mas de 50 caracteres',
            'carrera.required' => 'Campo requerido',
            'carrera.max' => 'Contenido de carrera no debe tener mas de 30 caracteres',
            'apellido_p.required' => 'Campo requerido',
            'apellido_p.max' => 'El apellido paterno no debe tener mas de 50 caracteres',
            'apellido_m.required' => 'Campo requerido',
            'apellido_m.max' => 'El apellido materno no debe tener mas de 50 caracteres',
            'email.required' => 'Campo requerido',
            'email.max' => 'El  email no debe tener mas de 200 caracteres',
            'nombre.required' => 'Campo de nombre requerido',
            'nombre.max' => 'El nombre no debe tener mas de 50 caracteres',
            'rol.required' => 'Campo de rol requerido',
            'rol.max' => 'El rol no debe tener mas de 50 caracteres',
        ];

        $validator = Validator::make($request->all(),$rules,$msg);
        if($validator->fails()){
            return response()->json($validator->messages(), 400);
        }
        $usuarioBL = new UsuarioBL();
        $respuesta = $usuarioBL->nuevoUsuario($request);
        return $respuesta;
    }

    /**
     * @OA\Get(
     *     path="/api/usuarios/{id}",
     *     tags={"Usuarios"},
     *     summary="Obtener un Usuario",
     *     @OA\Parameter(
     *         name="ci",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int32"
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Busqueda del usuario exitosa"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ohhh no !! Ha ocurrido un error en mostrar un Usuario en especifico."
     *     )
     * )
     */
    public function show($ci)
    {
        $usuarioBL = new UsuarioBL();
        $respuesta = $usuarioBL->mostrarUsuario($ci);
        return $respuesta;
    }

         /**
     * @OA\Post(
     * 		path="/api/usuarios/update",
     *      summary="Modificacion",
     * 		tags={"Usuarios"},
     * 		operationId="ModificacionUsuarios",
     * 		summary="Api documentada para la modificacion de un usuario",
     *      @OA\Parameter(
     *           name="ci",
     *           in="path",
     *           required=true,
     *            @OA\Schema(
     *              type="string",
     *              format="int32"
     *            ),
     *            style="form"
     *          ),
     *      @OA\Response(
     *              response=200,
     *              description="Modificacion del usuario exitosa"
     *        ),
     *       @OA\Response(
     *            response="default",
     *            description="Ohhh NO!! Error al modificar al usuario"
     * 	    	),
     * 	)
     *
     */
    public function update(Request $request)
    {
        $usuarioBL = new UsuarioBL();
        $respuesta = $usuarioBL->actualizarUsuario($request);
        return $respuesta;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Put(
     *     path="/api/usuarios/{ci}",
     *     tags={"Usuarios"},
     *     summary="Eliminir a un Usuario",
     *     @OA\Parameter(
     *         name="ci",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             format="int32"
     *         ),
     *         style="form"
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Eliminacion del Usuario exitosa"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ohhh NO!! Error al eliminar al usuario",
     *     )
     * )
     */
    public function delete($ci)
    {
        $usuarioBL = new UsuarioBL();
        $respuesta = $usuarioBL->borrarUsuario($ci);
        return $respuesta;
    }
}