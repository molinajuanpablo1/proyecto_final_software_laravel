<?php

namespace App\Http\DAO;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use App\UsuarioModel;
use Illuminate\Support\Facades\Hash;

class UsuarioDAO
{
    function obtenerUsuarios(){
        try{
            $usuario= array();
            foreach(UsuarioModel::all()->sortBy('nickName') as $usuar){
                $usuario[] = [
                    'ci'=>$usuar->ci,
                    'nickName'=>$usuar->nickName,
                    'carrera'=>$usuar->carrera,
                    'email'=>$usuar->email,
                    'rol'=>$usuar->rol,
                ];
            }
            return response()->json(
                 $usuario, 200);
        }catch (Exception $e){
            return response()->json([
                'Error'=> 'Error en la base de datos',
            ], $e->getCode());
        }
    }
    function crearUsuario($request){
        try{
            if ($request->hasFile('foto_perfil')){

                $file=$request->file('foto_perfil');
                $nameimg=time().$file->getClientOriginalName();
                $file->move(public_path().'/images/', $nameimg);
            }
            $usuario = new UsuarioModel();
            $usuario->ci=$request->input('ci');
            $usuario->password=hash::make($request->input('password'));
            $usuario->nickName=$request->input('nickName');
            $usuario->carrera=$request->input('carrera');
            $usuario->apellido_p=$request->input('apellido_p');
            $usuario->apellido_m=$request->input('apellido_m');
            $usuario->email=$request->input('email');
            $usuario->nombre=$request->input('nombre');
            $usuario->rol=$request->input('rol');
            $usuario-> save();
            return response()->json([
                'Mensaje'=> 'Usuario guardado',
            ], 200);
        }catch (Exception $e){
            return response()->json([
                'Error'=> 'Error en la base de datos',
            ], $e->getCode());
        }
    }
    function obtenerUsuario($ci){
        try{
            $usuario = UsuarioModel::findOrFail($ci);
            return response()->json(['usuario'=>$usuario],200);

        }catch (ModelNotFoundException $exception){
            return response()->json([
                'ErrorModelo'=> 'Usuario no encontrado',
            ], 404);
        }
    }
    function actualizarUsuario($request){
        try{
            UsuarioModel::findOrFail($request['ci'])->update($request->toArray());
            return response()->json(['status'=>'Usuario actualizado correctamente'],200);
        } catch (ModelNotFoundException $exception){
            return response()->json([
                'ErrorDB'=> 'Error interno',
            ], 500);
        } catch (\Exception $exception){
            return response()->json([
                'Error'=> 'Error al guardar el usuario',
            ], $exception->getCode());
        }
    }
    function borrarUsuario($ci){
        try{
            $usuario = UsuarioModel::findOrFail($ci)->delete();
            return response()->json(['msg'=>'Usuario eliminado'],200);

        }catch (ModelNotFoundException $exception){
            return response()->json([
                'ErrorModelo'=> 'Usuario no encontrado',
            ], 401);
        }
    }
    function list($request){
        try
        {
            $usuario = UsuarioModel::where('ci', '<>', $request->nickName)
                ->get();
                
            return response()->json($usuario, 200);
        }
        
        catch (Exception $e)
        {
            return response()->json([
                'Error'=> 'Ha ocurrido un error',
            ], 400);
        }
    }
    

    function search($request){
        try
        {
            $filtro = UsuarioModel::
                where('nickName', 'LIKE', '%'.$request->ci.'%')
                ->orwhere('ci', 'LIKE', '%'.$request->ci.'%')
                ->orwhere('carrera', 'LIKE', '%'.$request->ci.'%')
                ->orderBy('nickName','asc')
                ->get();
   //             ->paginate(10);
            
                return response()->json($filtro, 200);
        }
        
        catch (Exception $e)
        {
            return response()->json([
                'Error'=> 'Ha ocurrido un error',
            ], 400);
        }
    }
}