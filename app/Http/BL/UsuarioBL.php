<?php

namespace App\Http\BL;


use App\Http\DAO\UsuarioDAO;

class UsuarioBL
{
    /*function listarUsuarios(){
        $usuariosDAO = new UsuarioDAO();
        return $usuariosDAO->obtenerUsuarios();
    }*/
    function pedirUsuario($ci, $type){
        $UsuarioDAO = new UsuarioDAO();
        if($type == 'buscar'){
            $userResp = $UsuarioDAO->search($ci);
            return $userResp;
        }
        else{
            $userResp = $UsuarioDAO->list($ci);
            return $userResp;
        }
    }
    function nuevoUsuario($request){
        $usuariosDAO = new UsuarioDAO();
        return $usuariosDAO->crearUsuario($request);
    }
    function mostrarUsuario($ci){
        $usuariosDAO = new UsuarioDAO();
        return $usuariosDAO->obtenerUsuario($ci);
    }
    function actualizarUsuario($request){
        $usuariosDAO = new UsuarioDAO();
        return $usuariosDAO->actualizarUsuario($request);
    }
    function borrarUsuario($ci){
        $usuariosDAO = new UsuarioDAO();
        return $usuariosDAO->borrarUsuario($ci);
    }
}