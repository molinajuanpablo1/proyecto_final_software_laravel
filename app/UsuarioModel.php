<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'ci';
    public $timestamps = false;
    protected $fillable = ["password","nickName", "carrera", "foto_perfil",
                           "apellido_p","apellido_m", "email", "nombre","rol"];
}
