<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
//configuracion para JWT:
Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});
/*
 * PARA consultar primero por un jwt despues de hacer la autentificacion
 * se debe encerrar dentro de un grupo el cual necesite un middleware
 * a continuacion solo un ejemplo.
 * para que funcione comentar la ruta de la api de denuncias en la linea 45
 *
*/
Route::middleware(['jwt.auth'])->group(function (){
    //Rutas para Usuarios
    
});

Route::post('usuarios', 'UsuarioController@list');
Route::post('usuariosc', 'UsuarioController@create');
Route::get('usuarios/{ci}', 'UsuarioController@show');
Route::put('usuarios/', 'UsuarioController@update');
Route::delete('usuarios/{ci}', 'UsuarioController@delete');